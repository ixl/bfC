#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "state.h"

state new_state(FILE *fout)
{
  state s;
  s.count = 0;
  s.current = NUL;
  s.fout = fout;
  s.indent = 1;
  return s;
}

void emit_op(state *s, char c);
void set_state_to_INC(state *s, char c);
void set_state_to_MOV(state *s, char c);
void flush_state(state *s);

void emit_indent(state *s);

void update_state(state *s, char c)
{
  switch (s->current)
  {
  case NUL:
    switch (c)
    {
    case ',':
    case '.':
      emit_op(s, c);
      break;
    case '[':
      emit_op(s, c);
      s->indent++;
      break;
    case ']':
      flush_state(s);
      s->current = NUL;
      s->indent--;
      emit_op(s, c);
      break;
    case '+':
    case '-':
      set_state_to_INC(s, c);
      break;
    case '>':
    case '<':
      set_state_to_MOV(s, c);
      break;
    }
    break;
  case INC:
    switch (c)
    {
    case '+':
      s->count++;
      break;
    case '-':
      s->count--;
      break;
    case ',':
    case '.':
      emit_op(s, c);
      s->current = NUL;
      break;
    case '[':
      emit_op(s, c);
      s->indent++;
      s->current = NUL;
      break;
    case ']':
      flush_state(s);
      s->current = NUL;
      s->indent--;
      emit_op(s, c);
      break;
    case '<':
    case '>':
      emit_op(s, c);
      set_state_to_MOV(s, c);
      break;
    }
    break;
  case MOV:
    switch (c)
    {
    case '>':
      s->count++;
      break;
    case '<':
      s->count--;
      break;
    case ',':
    case '.':
      emit_op(s, c);
      s->current = NUL;
      break;
    case '[':
      emit_op(s, c);
      s->indent++;
      s->current = NUL;
      break;
    case ']':
      flush_state(s);
      s->current = NUL;
      s->indent--;
      emit_op(s, c);
      break;
    case '+':
    case '-':
      emit_op(s, c);
      set_state_to_INC(s, c);
      break;
    }
    break;
  }
}

void emit_header(state *s)
{
  fprintf(s->fout,
          "#include <stdio.h>\n"
          "#include <stdlib.h>\n\n"
          "#define SIZE 1337\n"
          "int main() {\n");
  emit_indent(s);
  fprintf(s->fout, "uint8_t *ptr = malloc(SIZE);\n");
  emit_indent(s);
  fprintf(s->fout, "ptr = ptr + SIZE / 2;\n");
}

void emit_footer(state *s)
{
  emit_indent(s);
  fprintf(s->fout, "return 0;\n}\n");
}

void emit_op(state *s, char c)
{
  flush_state(s);
  switch (c)
  {
  case '.':
    emit_indent(s);
    fprintf(s->fout, "putchar(*ptr);\n");
    break;
  case ',':
    emit_indent(s);
    fprintf(s->fout, "*ptr=getchar();\n");
    break;
  case '[':
    emit_indent(s);
    fprintf(s->fout, "while (*ptr) {\n");
    break;
  case ']':
    emit_indent(s);
    fprintf(s->fout, "}\n");
    break;
  }
}

void emit_indent(state *s)
{
  // not even checking errors
  char *ind = malloc(s->indent + 1);
  memset(ind, '\t', s->indent);
  ind[s->indent] = '\0';
  fprintf(s->fout, "%s", ind);
  free(ind);
}

void flush_state(state *s)
{
  if (s->current == NUL)
  {
    return;
  }
  if (s->current == INC)
  {
    emit_indent(s);
    fprintf(s->fout,
            "*ptr = *ptr + (%d);\n",
            s->count);
    return;
  }
  if (s->current == MOV)
  {
    emit_indent(s);
    fprintf(s->fout,
            "ptr = ptr + (%d);\n",
            s->count);
    return;
  }
}

void set_state_to_INC(state *s, char c)
{
  s->count = 0;
  s->current = INC;
  if (c == '+')
    s->count++;
  if (c == '-')
    s->count--;
}

void set_state_to_MOV(state *s, char c)
{
  s->count = 0;
  s->current = MOV;
  if (c == '>')
    s->count++;
  if (c == '<')
    s->count--;
}
