#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "state.h"

int main()
{
  // todo parse flags
  FILE *fin = stdin;
  FILE *fout = stdout;

  state s = new_state(fout);

  emit_header(&s);
  char c;
  while ((c = fgetc(fin)) != EOF)
  {
    update_state(&s, c);
  }

  emit_footer(&s);
  fclose(fout);
  return 0;
}
