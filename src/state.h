#pragma once

enum CURR_SYMBOL
{
  MOV,
  INC,
  NUL,
};

typedef struct
{
  enum CURR_SYMBOL current;
  int count;
  // TODO add indentation
  size_t indent;
  FILE *fout;
} state;

state new_state(FILE *fout);

void update_state(state *s, char c);

void emit_header(state *s);
void emit_footer(state *s);