# Brainfuck to C transpiler

optimizes code to reduce size

To compile the transpiler just
```bash
make compile.out
```

To transpile example/hello.bf (hello world in brainfuck):

```bash
make example
```

This will output hello.c in example directory