src = $(wildcard src/*.c)
inc = $(wildcard src/*.h)

example: example/hello.out


compile.out: $(src) $(inc)
	cc -Wall $(src) -o $@

g: $(src) $(inc)
	cc -Wall -g src/*.c -o compile.out


example/hello.out: compile.out example/hello.bf
	cp compile.out example
	$(MAKE) -C example hello.out


clean: compile.out
	-rm $^
	$(MAKE) -C example clean
